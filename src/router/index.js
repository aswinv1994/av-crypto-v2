import VueRouter from "vue-router";
import routes from "./routes";
import firebase from 'firebase'

// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  linkExactActiveClass: "active",
  scrollBehavior: (to) => {
    if (to.hash) {
      return {selector: to.hash}
    } else {
      return { x: 0, y: 0 }
    }
  }
});

router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if( requiresAuth && !currentUser) next('signin');
  else if(!requiresAuth && currentUser) next('dashboard');
  else next();
});

export default router;
