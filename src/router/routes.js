import DashboardLayout from "@/layout/dashboard/DashboardLayout.vue";
// GeneralViews
import NotFound from "@/pages/NotFoundPage.vue";

// Admin pages
const Dashboard = () => import(/* webpackChunkName: "dashboard" */"@/pages/Dashboard.vue");
const Profile = () => import(/* webpackChunkName: "common" */ "@/pages/Profile.vue");
const Notifications = () => import(/* webpackChunkName: "common" */"@/pages/Notifications.vue");
const Icons = () => import(/* webpackChunkName: "common" */ "@/pages/Icons.vue");
const Maps = () => import(/* webpackChunkName: "common" */ "@/pages/Maps.vue");
const Typography = () => import(/* webpackChunkName: "common" */ "@/pages/Typography.vue");
const TableList = () => import(/* webpackChunkName: "common" */ "@/pages/TableList.vue");

const Signin = () => import(/* webpackChunkName: "common" */ "@/pages/Signin.vue");
const Signup = () => import(/* webpackChunkName: "common" */ "@/pages/Signup.vue");


const routes = [
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/signin",
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        component: Dashboard,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "profile",
        name: "profile",
        component: Profile,
          meta: {
              requiresAuth: true
          }
      },
      {
        path: "notifications",
        name: "notifications",
        component: Notifications
      },
      {
        path: "crypto-news",
        name: "Crypto News",
        component: Icons,
          meta: {
              requiresAuth: true
          }
      },
      {
        path: "maps",
        name: "maps",
        component: Maps
      },
      {
        path: "typography",
        name: "typography",
        component: Typography
      },
      {
        path: "table-list",
        name: "Crypto List",
        component: TableList,
          meta: {
              requiresAuth: true
          }
      },
        {
          path: "signin",
          name: "signin",
          component: Signin
        },
        {
            path: "signup",
            name: "signup",
            component: Signup
        }

    ]
  },
  { path: "*", component: NotFound },
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
