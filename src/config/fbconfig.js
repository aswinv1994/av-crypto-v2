import firebase from 'firebase/app'
import 'firebase/auth'

// Initialize Firebase
var config = {
    apiKey: "AIzaSyA26Lwx6EOPXyfSM3OVhNuCI1eJypaIPRU",
    authDomain: "av-crypto.firebaseapp.com",
    databaseURL: "https://av-crypto.firebaseio.com",
    projectId: "av-crypto",
    storageBucket: "av-crypto.appspot.com",
    messagingSenderId: "832867667457"
};

firebase.initializeApp(config);

export default firebase